package com.di;

import com.di.annotations.Inject;
import com.di.exceptions.BindingNotFoundException;
import com.di.exceptions.ConstructorNotFoundException;
import com.di.exceptions.TooManyConstructorsException;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InjectorImpl implements Injector {

    private Map<Class, ServicesBuilder> bindingMap = new HashMap<>();

    @SuppressWarnings("unchecked")
    public synchronized <T> Provider<T> getProvider(Class<T> type) {
        final ServicesBuilder<T> servicesBuilder = (ServicesBuilder<T>) bindingMap.get(type);
        if (servicesBuilder != null) {
            return () -> {
                T instance;
                // to be sure one instance of singleton service will be created
                synchronized (Injector.class) {
                    instance = servicesBuilder.getInstance() == null ?
                            newInstance(servicesBuilder.getClassImpl()) :
                            servicesBuilder.getInstance();
                    //cache
                    if (servicesBuilder.getInstance() == null && servicesBuilder.isSingleton()) {
                        servicesBuilder.setInstance(instance);
                    }
                }
                return instance;
            };
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private <T> T newInstance(Class<T> classImpl) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<T> constructor = getConstructor(classImpl);
        Class<?>[] parameterTypes = constructor.getParameterTypes();
        Object[] objects = new Object[parameterTypes.length];
        if (parameterTypes.length > 0) { // Is there any parameters in the constructor
            for (int i = 0; i < parameterTypes.length; i++) {
                if (bindingMap.get(parameterTypes[i]) == null) { // Is there for this parameter binding
                    throw new BindingNotFoundException("-------Binding not found Exception --------");
                }
                objects[i] = getProvider(parameterTypes[i]).getInstance();
            }
        }
        return constructor.newInstance(objects);
    }

    public <T> void bind(Class<T> intf, Class<? extends T> impl) {
        isValidClass(impl); // Scan to see if there are any suitable constructors
        bindingMap.put(intf, ServicesBuilder.prototype(impl));
    }

    public <T> void bindSingleton(Class<T> intf, Class<? extends T> impl) {
        if (bindingMap.containsKey(intf) && bindingMap.get(intf).getClassImpl() == impl) { // If need to change the implementation
                return;
        }
        isValidClass(impl); // Scan to see if there are any suitable constructors
        bindingMap.put(intf, ServicesBuilder.singleton(impl));
    }

    private <T> Constructor getConstructor(Class<T> type) {
        Constructor[] constructors = type.getConstructors();
        List<Constructor> constructorsWithAnnotationInjectOrDefault = Stream.of(constructors).filter(s -> s.getAnnotation(Inject.class) != null).collect(Collectors.toList()); // Search for a constructor with an annotation Inject
        if (constructorsWithAnnotationInjectOrDefault.size() == 0) { // If there are no constructors with annotation Inject, take the default
            constructorsWithAnnotationInjectOrDefault = Stream.of((constructors)).filter(c -> c.getParameterCount() == 0).collect(Collectors.toList());
        }
        return constructorsWithAnnotationInjectOrDefault.get(0);
    }

    private <T> void isValidClass(Class<T> type) {
        Constructor[] constructors = type.getConstructors();
        long countConstructors = Stream.of(constructors).filter(s -> s.getAnnotation(Inject.class) != null).count();
        if (countConstructors == 0) { // If there are no constructors with annotation Inject, checking if there is a default
            countConstructors = Stream.of((constructors)).filter(c -> c.getParameterCount() == 0).count();
            if (countConstructors == 0) { // If there is no default, an exception is thrown
                throw new ConstructorNotFoundException("------Constructor not found Exception------");
            }
        } else if (countConstructors > 1) {
            throw new TooManyConstructorsException("------Too many constructors Exception------");
        }
    }
}

