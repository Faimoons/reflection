package com.di;

public class ServicesBuilder<T> {

    private final Class<T> classImpl;
    private final boolean isSingleton;
    private T instance;

    public ServicesBuilder(Class<T> classImpl, boolean isSingleton) {
        this.classImpl = classImpl;
        this.isSingleton = isSingleton;
    }


    public static <T> ServicesBuilder singleton(Class<T> impl){
        return new ServicesBuilder<>(impl,true);
    }


    public static <T> ServicesBuilder prototype(Class<T> impl){
        return new ServicesBuilder<>(impl,false);
    }

    public Class<T> getClassImpl() {
        return classImpl;
    }

    public boolean isSingleton() {
        return isSingleton;
    }

    public T getInstance() {
        return instance;
    }

    public void setInstance(T instance) {
        this.instance = instance;
    }
}
