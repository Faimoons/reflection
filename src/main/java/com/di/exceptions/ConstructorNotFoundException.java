package com.di.exceptions;

public class ConstructorNotFoundException extends RuntimeException {

    public ConstructorNotFoundException(String massage) {
        super(massage);
    }

}
