package com.di;

import java.lang.reflect.InvocationTargetException;

@FunctionalInterface
public interface Provider<T> {

    T getInstance() throws IllegalAccessException, InstantiationException, InvocationTargetException;

}
