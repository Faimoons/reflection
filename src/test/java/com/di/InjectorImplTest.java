package com.di;

import com.di.test.*;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

public class InjectorImplTest {

    @Test
    public void testExistingBinding() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Injector injector = new InjectorImpl();
        injector.bind(EventDAO.class, EventDAOImpl.class);
        injector.bind(EventService.class, EventServiceImpl.class);

        Provider<EventService> provider = injector.getProvider(EventService.class);

        assertNotNull(provider);
        assertNotNull(provider.getInstance());
        assertSame(EventServiceImpl.class, provider.getInstance().getClass());
    }

    @Test
    public void testNotExistBinding() {
        Injector injector = new InjectorImpl();
        Provider<EventService> provider = injector.getProvider(EventService.class);
        Assert.assertNull(provider);
    }
}
