package com.di.test;

import com.di.annotations.Inject;

public class EventServiceImpl implements EventService {

    private EventDAO eventDao;

    @Inject
    public EventServiceImpl(EventDAO eventDao) {
        this.eventDao = eventDao;
    }

    public EventServiceImpl() {
    }

    public EventDAO getEventDao() {
        return eventDao;
    }

    public void setEventDao(EventDAO eventDao) {
        this.eventDao = eventDao;
    }
}
