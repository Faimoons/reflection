package com.di.test;

import com.di.annotations.Inject;

public class EventServicesTooManyConstructor implements EventService {

    private EventDAO eventDao;

    @Inject
    public EventServicesTooManyConstructor(EventDAO eventDao) {
        this.eventDao = eventDao;
    }

    @Inject
    public EventServicesTooManyConstructor() {
    }

    @Override
    public EventDAO getEventDao() {
        return eventDao;
    }
}
