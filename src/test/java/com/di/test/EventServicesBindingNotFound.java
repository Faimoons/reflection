package com.di.test;

import com.di.annotations.Inject;

public class EventServicesBindingNotFound implements EventService {
    private EventDAO eventDao;

    @Inject
    public EventServicesBindingNotFound(EventDAO eventDao, EventServiceImpl eventServiceImpl){
        this.eventDao = eventDao;
    }

    public EventServicesBindingNotFound() {
    }

    @Override
    public EventDAO getEventDao() {
        return eventDao;
    }
}
