package com.di.exceptions;

import com.di.Injector;
import com.di.InjectorImpl;
import com.di.test.EvenDAOWithoutDefaultConstructor;
import com.di.test.EventDAO;
import com.di.test.EventService;
import com.di.test.EventServiceImpl;
import org.junit.Test;

public class ConstructorNotFoundExceptionTest {

    @Test(expected = ConstructorNotFoundException.class)
    public void testConstructorNotFoundException() {
        Injector injector = new InjectorImpl();
        injector.bind(EventDAO.class, EvenDAOWithoutDefaultConstructor.class);
        injector.bind(EventService.class, EventServiceImpl.class);
    }
}
