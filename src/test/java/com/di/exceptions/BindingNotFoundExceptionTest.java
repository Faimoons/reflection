package com.di.exceptions;

import com.di.Injector;
import com.di.InjectorImpl;
import com.di.Provider;
import com.di.test.EventDAO;
import com.di.test.EventDAOImpl;
import com.di.test.EventService;
import com.di.test.EventServicesBindingNotFound;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

public class BindingNotFoundExceptionTest {

    @Test(expected = BindingNotFoundException.class)
    public void testBindingNotFoundException() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Injector injector = new InjectorImpl();
        injector.bind(EventDAO.class, EventDAOImpl.class);
        injector.bind(EventService.class, EventServicesBindingNotFound.class);
        Provider<EventService> provider = injector.getProvider(EventService.class);
        provider.getInstance();
    }
}
