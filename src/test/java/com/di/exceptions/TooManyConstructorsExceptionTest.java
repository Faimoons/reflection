package com.di.exceptions;

import com.di.Injector;
import com.di.InjectorImpl;
import com.di.test.EventDAO;
import com.di.test.EventDAOImpl;
import com.di.test.EventService;
import com.di.test.EventServicesTooManyConstructor;
import org.junit.Test;

public class TooManyConstructorsExceptionTest {

    @Test(expected = TooManyConstructorsException.class)
    public void testTooManyConstructorsException() {
        Injector injector = new InjectorImpl();
        injector.bind(EventDAO.class, EventDAOImpl.class);
        injector.bind(EventService.class, EventServicesTooManyConstructor.class);
    }
}
