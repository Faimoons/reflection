package com.di;

import com.di.test.*;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

public class SingletonTest {

    @Test
    public void testClassWithAnnotationInject() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Injector injector = new InjectorImpl();

        injector.bind(EventDAO.class, EventDAOImpl.class);
        injector.bindSingleton(EventService.class, EventServiceImpl.class);

        Provider<EventService> provider = injector.getProvider(EventService.class);
        Provider<EventService> provider1 = injector.getProvider(EventService.class);

        Assert.assertEquals(provider.getInstance(), provider1.getInstance());
    }

    @Test
    public void testAllClassAsSingleton() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Injector injector = new InjectorImpl();

        injector.bindSingleton(EventDAO.class, EventDAOImpl.class);
        injector.bindSingleton(EventService.class, EventServiceImpl.class);

        Provider<EventService> provider = injector.getProvider(EventService.class);
        Provider<EventService> provider1 = injector.getProvider(EventService.class);

        Assert.assertEquals(provider.getInstance(), provider1.getInstance());
        Assert.assertEquals(provider.getInstance().getEventDao(),provider1.getInstance().getEventDao());
    }

    @Test
    public void testConstructorWithParameterAsSingleton() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Injector injector = new InjectorImpl();

        injector.bindSingleton(EventDAO.class, EventDAOImpl.class);
        injector.bind(EventService.class, EventServiceImpl.class);

        Provider<EventService> provider = injector.getProvider(EventService.class);
        Provider<EventService> provider1 = injector.getProvider(EventService.class);

        Assert.assertEquals(provider.getInstance().getEventDao(), provider1.getInstance().getEventDao());
        Assert.assertNotEquals(provider.getInstance(),provider1.getInstance());
    }
}
